(ns clj-algorithms.graphs)

;;; Graph structure is represented as an adjacency list key-value map:
;;;
;;;      {:a (:b :c :d) :b () :c (:d) :d ()}
;;;
;;; Every key element is a vertex, every value element is a list of
;;; connected vertexes.

;;; We can easily extend this concept for weighted graphs:
;;;
;;;     {:a ((:b 10) (:c 5) (:d 13)) :b () :c ((:d 3)) :d ()}
;;;
;;; In this case adjacency list is a list of lists each of which is an
;;; edge with a vertex label in the head and weight (and potentially
;;; other parameters in the tail.

;;; Undirected graph is a directed graph with edges going both ways.

;;; Notation: g, G are graphs, v,w,s,t are vertexes, e,d are edges

(defn make-graph
  "Creates graph out of list of vertexes vs.
Out of list (:a :b :c) it will return (:a ()"
  [vs]
  (zipmap vs (repeat '())))

(defn add-edge
  "Adds edge from v to w in the graph. Vertexes v and w should be present in the graph g. Arguments passed after w are set as a properties of the edge, e.g. weight or distance."
  [g v w & xs]
  (if (and (contains? g v)
           (contains? g w))
    (update-in g [v] (partial cons (if xs (cons w xs) w)))))

(defn connect
  "Connects two vertices v and w, i. e. adds two edges v -> w and w -> v, in the graph g. Just a handy tool for creating undirected graphs"
  [g v w]
  (-> g
    (add-edge v w)
    (add-edge w v)))

(defn dot
  "Returns a string in DOT language describing given graph"
  [g]
  (str
   ;; "graph " (:name (meta (var ))) " {"
   "graph g {"
   (->> (for [[v vs] g]
          (map #(str v " -- " % ";\n") vs))
        (apply concat)
        (apply str))
   "}"))

;;;
;;; Breadth-first-search
;;; We produce lazy sequence of vertices as a path. There are
;;; multiple possible paths in the graph depending on how we choose
;;; the edge. For instance, if we choose edge using shortest length
;;; edge criterion we will get the dijkstra shortest path algorithm
;;;
;;; f-edge-selector is a function that given adjacency-list sequence
;;; of edges produces the sequence of vertices/edges in order they
;;; should be put on the queue.
;;;
#_(defn breadth-first-search
  [g s f-edge-selector])

(def cg
  (-> (make-graph '(1 2 3 4 5 6))
      (connect 1 4)
      (connect 1 2)
      (connect 1 5)
      (connect 2 5)
      (connect 2 3)
      (connect 2 6)
      (connect 3 6)
      (connect 5 6)))

#_(defn transitions
  "Finds all the transitions going to and from the node. Transition to the node are reversed."
  [graph node]
  (map #(if (= node (first %))
          %
          (reverse %))
       (filter #(or (= node (first %)) (= node (second %))) graph)))

(defn out-edges
  [g v]
  (map #(list v %) (g v)))

(defn breadth-first
  "Returns a lazy sequence of transitions in breadth-first order starting from the given node."
  [g s]
  (let [walk
        ;; edges are out edges
        ;; (:a :b)
        ;;  visited is 
        (fn walk [visited edges]
          (let [es (remove #(visited (second %)) edges)]
            (when-let [v-next (second (first es))]
              (lazy-seq
               (cons (first es)
                     (walk (conj visited v-next)
                           (concat es (out-edges g v-next))))))))]
    (walk #{s} (out-edges g s))))

;;; returns lazy tree structure from the graph, that could be walked
;;;
;;;   {:a (:b :c) :b (:a :c) :c (:b :a)}
;;;
;;; starting :a should return
;;;
;;;   (:a (:b) (:c))

#_(defn dfs
  [g s visited]
  (lazy-cat
   (mapcat #(dfs g % (conj visited s))
           (remove #(contains? visited %) (g s)))))

;;; TODO: add limit on the search (function of the vertex?)
#_(defn depth-first-search
  "Explores graph using depth first search from vertex s. f is a function being dragged along the graph."
  [g s])

#_(defn- dfs
  [graph goal]
  (fn search
    [path visited]
    (let [current (peek path)]
      (if (= goal current)
        [path]
        (->> current graph keys
             (remove visited)
             (mapcat #(search (conj path %) (conj visited %))))))))

#_(defn findpath
  "Returns a lazy sequence of all directed paths from start to goal
  within graph."
  [graph start goal]
  ((dfs graph goal) [start] #{start}))
