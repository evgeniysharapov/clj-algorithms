# clj-algorithms

As the saying goes "The best way to learn something is just to start doing it".
So, here we go, to learn classic algorithms I decided to implement them in Clojure.

## Usage

Whatever you find useful, feel free to use wherever.

## License

Copyright (c) 2013 FIXME

Distributed under the Eclipse Public License, the same as Clojure.
